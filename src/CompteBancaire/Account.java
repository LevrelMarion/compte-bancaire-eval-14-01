package CompteBancaire;

public class Account {

    protected MonetaryAmount monetaryAmount;
    protected String ownerName = "Obélix";


    // currency = quantité d'argent dispo
    public Account(String ownerName, String currency) {
        monetaryAmount = new MonetaryAmount(0, currency);
        this.ownerName = ownerName;
    }

    // déposer de l'argent sur le compte
    public void deposit(double amount) {
        monetaryAmount.addAmount(amount);
    }

    // retirer de l'argent sur le compte  on ne peu pas retirer plus d'argent qu'il n'y en a de disponible sur le compte
    public void withdraw(double amount) {
        if (monetaryAmount.getAmount() > amount) {
            monetaryAmount.substractAmount(amount);
        } else {
            System.out.println("Somme inssufisante");
        }
    }

    // récupérer le montant actuel sur le compte
    public MonetaryAmount getCurrentBalance() {
        return monetaryAmount;
    }

    // Récupérer le nom du propriétaire du compte
    public String getOwnerName() {
        return ownerName;
    }
}
