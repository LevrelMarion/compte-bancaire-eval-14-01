package CompteBancaire;

public class MonetaryAmount {
    protected double moneyQuantity;
    protected String devise;

    // Constructeur de la classe Montant monétaire = quantité de fric + la devise Sesterses
    public MonetaryAmount(double amount, String currency) {
        this.devise = currency;
        this.moneyQuantity = amount;
    }

    public String getDevise() {
        return devise;
    }

    // ajouter de l'argent : variable double a + moneyQuantity
    public void addAmount(double a) {
        moneyQuantity += a;
    }

    // retirer de l'argent (le total peut être négatif)
    public void substractAmount(double a) {
             moneyQuantity -= a;

    }
    // convertit le total de l'argent en string
    public String getToString() {
        return Double.toString(moneyQuantity);
    }

    // obtenir le total dispo dans le porte monnaie
    public Double getAmount() {
        return moneyQuantity;
    }

}
