package CompteBancaire;

public class Main {

    public static void main(String[] args) {


        // 1.
        Account mainAccount = new Account("Obélix", "Sesteres");
        String name = mainAccount.getOwnerName();
        System.out.println("Propriétaire du compte bancaire : " + name);

        // 2.Récupérer le montant dispo sur le compte : il y en a pas...
        System.out.println("Montant disponible sur le compte : " + mainAccount.getCurrentBalance().getToString());

        // 3.déposer 500.5 sur le compte en appelant la méthode deposit
        mainAccount.deposit(500.5);
        System.out.println("Montant disponible sur le compte après ajout : " + mainAccount.getCurrentBalance().getToString());
        mainAccount.withdraw(125.25);
        System.out.println("Montant disponible sur le compte après retrait : " + mainAccount.getCurrentBalance().getToString());
        mainAccount.withdraw(1337);

        System.out.println("Montant disponible sur le compte après retrait : " + mainAccount.getCurrentBalance().getToString());


    }
}
